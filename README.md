# Feature Request Tracker

This script allows to collect data from a list of feature requests issues and generates a HTML repport.

## Usage
- tested with python 3.11
- setup an environment variable with your access token `export GITLAB_TOKEN=yOurRAcceSStoKen`
- it is recommanded to use a virutal env
- install requirements `pip3 install -r requirements.txt`
- at the root of the directory, create a `./issues.txt` file with the following structure. The string after the coma will be used in the `priority` column of the repport
 ```plantext
    https://gitlab.com/gitlab-org/gitlab/-/issues/26618,HIGH
    https://gitlab.com/gitlab-org/gitlab/-/issues/27133,HIGH
    https://gitlab.com/gitlab-org/gitlab/-/issues/29044,HIGH
    https://gitlab.com/gitlab-org/gitlab/-/issues/410472,HIGH
    ...
```

- run `python3 main.py` 
- the repport will be created in `./html/repport.html`
- at first run, the step collecting group labels takes time. The result is cache in the file `./group_labels_cache.bin`. You can delete it if you want to clear the cache.

## Data collected

The repport will have the following columns
- piority
- link to the issue
- Stage, group, and product manager name (if the script manages to bind stage and group to [this source](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml))
- related milestone if exists
- related epic if exists
- value of the `workflow::` label if exists

## Current limitations
- can only fetch issues from `https://gitlab.com/gitlab-org/gitlab`
- no csv export, but the repport can be easly copied/pasted to google sheets

"""
This Python code retrieves issue metadata from GitLab, enriches it with additional info, and generates a report. 
The key steps are using the GitLab API, caching common data like labels, and templating the output as HTML.


It does the following main steps:

Defines several helper functions:
find_info_from_labels - Gets info from a specific label on a GitLab issue
format_date - Formats a date string into a nicer format
get_issue_details - Gets all the details for a GitLab issue
get_group_labels - Gets all labels for a GitLab group (cached)
get_label_description_by_label_name - Gets a label's description by name
get_stages - Loads stage definitions from a YAML file


Sets up logging and creates a GitLab API client instance

Gets the group labels and stages from YAML

Reads a list of issues from issues.txt

Loops through each issue and calls get_issue_details to retrieve info

Uses Jinja2 to render the issue details into an HTML report

Writes the HTML report to a file


Usage:
    python main.py

"""

import datetime
import re
import logging
import os
import pickle
from gitlab import Gitlab
from jinja2 import Environment, FileSystemLoader
import requests
import yaml


def find_info_from_labels(issue, label_name):
    """
    Find the info from the labels

    Args:
        issue (obj) : The issue object  from the Gitlab API
        label_name (str) : The name of the label

    Returns:
        The info from the label
    """

    logger.info(f"Finding {label_name} from labels")
    try:
        # Define the regular expression pattern to fin the group in labels
        pattern = r"{label}::(.*)".format(label=label_name)

        # Extract the name from the string using the `re.search()` function
        match = re.search(
            pattern,
            [label for label in issue.labels if label.startswith(label_name + "::")][0],
        )
        return match.group(1)

    except:
        logger.info(f"Could not find {label_name} from labels")
        return None


def format_date(date):
    """
    Format the date

    Args:
        date (str): The date to format

    Returns:
        str: The formatted date"""
    # Create a datetime object from the given string
    date = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%fZ")

    # Extract the year, month, and day from the datetime object
    year = date.year
    month = date.month
    day = date.day

    # Create a new datetime object with only the year, month, and day
    return datetime.date(year, month, day)


def get_issue_details(issue_url, priority):
    """
    Get the issue details

    Args:
        issue_url (str): The URL of the issue
        priority (str): The priority of the issue

    Returns:
        dict: The issue details
    """

    logger.info(f"Getting {issue_url} details")

    global gitlab_instance
    global group_labels
    global stages

    # Get the issue ID from the URL
    issueId = issue_url.split("/")[-1]
    issue = gitlab_instance.projects.get("gitlab-org/gitlab").issues.get(id=issueId)

    # Get the issue details
    logger.info(f"Getting workflow details")
    try:
        group = find_info_from_labels(issue, "group").replace(" ", "_")
        stage = find_info_from_labels(issue, "devops").replace(" ", "_")
    except:
        group = None
        stage = None
    try:
        workflow_label_value = find_info_from_labels(issue, "workflow")
        workflow_details = get_label_description_by_label_name(
            label_name="workflow::" + workflow_label_value
        )
    except:
        workflow_details = "Workflow not started"

    logger.info(f"Finding group's product manager")

    # Get the product manager for the group
    try:
        pm = "PM: " + stages["stages"][stage]["groups"][group].get("pm")
    except:
        pm = "PM not found"

    return {
        "name": issue.title,
        "created_at": format_date(issue.created_at),
        "labels": issue.labels,
        "milestone": issue.milestone,
        "description": issue.description,
        "state": issue.state,
        "assignees": issue.assignees,
        "due_date": issue.due_date,
        "author": issue.author,
        "web_url": issue.web_url,
        "epic": issue.epic,
        "group": group,
        "workflow": workflow_label_value,
        "workflow_details": workflow_details,
        "issue": find_info_from_labels(issue, "issue"),
        "devops": stage,
        "pm": pm,
        "priority": priority,
    }


def get_group_labels(group_id):
    """
    Retrieve a list of labels for a GitLab group.

    Args:
        group_id (str): The ID of the group.

    Returns:
        list: A list of labels for the group.
    """

    # Check if the cache file exists
    cache_file = "group_labels_cache.bin"
    if os.path.exists(cache_file):
        # Load cached data from file
        with open(cache_file, "rb") as f:
            logger.info("Loading group labels from cache")
            cached_data = pickle.load(f)
        return cached_data

    # Get the group

    global gitlab_instance
    logger.info(f"Getting group {group_id} labels")
    group = gitlab_instance.groups.get(group_id)
    group_labels = group.labels.list(get_all=True)

    with open(cache_file, "wb") as f:
        pickle.dump(group_labels, f)

    return group_labels


def get_label_description_by_label_name(label_name):
    """
    Retrieve the description of a label by its name.

    Args:
        label_name (str): The name of the label.

    Returns:
        str: The description of the label.

    """

    global group_labels

    logger.info(f"Getting {label_name} description")

    for label in group_labels:
        if label.name == label_name:
            return label.description
    return None


def get_stages():
    """
    Retrieve the stages from the YAML file.

    Returns:
        A dictionary of stages.
    """

    url = "https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml"

    response = requests.get(url)

    if response.status_code == 200:
        # The request was successful, so parse the YAML data
        data = response.text
        return yaml.safe_load(data)


def main():
    """
    Main function
    """

    # Check if the environment variables are set
    if not os.getenv("GITLAB_TOKEN"):
        print(
            "Error: GITLAB_TOKEN environment variable is not set. Please set it before running the script. Go to https://gitlab.com/-/user_settings/personal_access_tokens to create one."
        )
        exit(1)

    # check if issues.txt file is present and exits if not
    if not os.path.exists("issues.txt"):
        print("Error: issues.txt file is not present. Please create one.")
        exit(1)

    # logging configuration
    global logger
    logger = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO)

    # Create a GitLab instance
    logger.info("Creating GitLab instance")
    global gitlab_instance
    group_id = "9970"
    gitlab_instance = Gitlab(
        url="https://gitlab.com", private_token=os.environ["GITLAB_TOKEN"]
    )

    # Get the group labels
    global group_labels
    group_labels = get_group_labels(group_id=group_id)

    # Get the stages
    global stages
    stages = get_stages()

    logger.info("Reading issues.txt")

    # Read the issues from the file
    issues = []

    with open("issues.txt") as f:
        for line in f:
            issue_url, priority = line.split(",")

            issue_details = get_issue_details(issue_url, priority)

            issues.append(issue_details)

    # create output reports using an HTML template

    logger.info("Creating HTML report")
    from jinja2 import Template

    # Create the HTML template
    root = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.join(root, "templates")
    env = Environment(loader=FileSystemLoader(templates_dir))
    template = env.get_template("index.html")

    # Specify the name of the folder and file
    folder_name = "html"
    file_name = "repport.html"

    # Check if the folder already exists
    if not os.path.exists(folder_name):
        # Create the folder if it doesn't exist
        os.makedirs(folder_name)

    # Delete the file if it already exists
    if os.path.exists(file_name):
        os.remove(file_name)

    # Create the file path
    file_path = os.path.join(root, folder_name, file_name)

    # render the template and write it to the file
    with open(file_path, "a") as fh:
        fh.write(template.render(issues=issues))


if __name__ == "__main__":
    main()
